/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Skynet
 */
public class Menu implements Serializable {

    private List<Preparacion> preparaciones;
    private List<Bebida> bebidas;

    public Menu() {
        preparaciones = new ArrayList<>();
        bebidas = new ArrayList<>();
    }

    public void agregar(Preparacion p) {
        if (p != null) {
            preparaciones.add(p);
        }
    }

    public void agregar(Bebida b) {
        if (b != null) {
            bebidas.add(b);
        }
    }

    void mostrar() {
        EntradaSalida.mostrarString("Carta del día de hoy \n BEBIDAS \n" + MostrarListadoBebidas() + "\n");
        EntradaSalida.mostrarString("PLATOS \n" + MostrarListadoPreparaciones() + "\n");
    }

    public String MostrarListadoBebidas() {
        String lista = "";
        for (int i = 0; i < bebidas.size(); i++) {
            lista += "[" + (i + 1) + "] " + bebidas.get(i).getDescripcion() + ": " + bebidas.get(i).getPrecio() + "$ \n";
        }
        return lista;
    }

    int obtenerCantidadPreparaciones() {
        return preparaciones.size();
    }

    int obtenerCantidadBebidas() {
        return bebidas.size();
    }

    public String MostrarListadoPreparaciones() {
        String lista = "";
        for (int i = 0; i < preparaciones.size(); i++) {
            lista += "[" + (i + 1) + "] " + preparaciones.get(i).getDescripcion() + ": " + preparaciones.get(i).getPrecio() + "$ \n";
        }
        return lista;
    }

    public String MostrarListadoPreparacionesVendibles() {
        String lista = "";
        for (int i = 0; i < preparaciones.size(); i++) {
            if (!(preparaciones.get(i).getPrecio().equals("0"))) {
                lista += "[" + (i + 1) + "] " + preparaciones.get(i).getDescripcion() + ": " + preparaciones.get(i).getPrecio() + "$ \n";
            }
        }
        return lista;
    }

    public Preparacion getPreparacion(int i) {
        return preparaciones.get(i);
    }

    public Bebida getBebida(int i) {
        return bebidas.get(i);
    }

    public List<Preparacion> getPreparaciones() {
        return preparaciones;
    }

    public List<Bebida> getBebidas() {
        return bebidas;
    }

}
