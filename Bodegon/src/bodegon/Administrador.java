/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

import java.io.IOException;

/**
 *
 * @author Skynet
 */
public class Administrador extends Persona {

    private String nombre;
    private String codigoAcceso;
    private String password;
    private String usuario;

    public Administrador(String usuario, String password, String codigoAcceso, String nombre) {
        super(usuario, password, codigoAcceso, nombre);
        this.nombre = nombre;
        this.codigoAcceso = codigoAcceso;
        this.usuario = usuario;
        this.password = password;
    }

    @Override
    public boolean proceder(Sistema s) {
        boolean seguir = true;
        String input;
        String user, passw, nom, cod;
        //while (!cerrar) {
        char op;
        do {
            op = EntradaSalida.leerChar(
                    "Hola, " + nombre + ".\n"
                    + "MENÚ ADMIN\n\n"
                    + "[1] Dar alta camarero\n"
                    + "[2] Dar alta cocinero\n"
                    + "[3] Agregar bebidas\n"
                    + "[4] Dar alta preparaciones\n"
                    + "[5] Revisar carta\n"
                    + "[6] Resumen de jornada\n"
                    + "[7] Cerrar sesión\n"
                    + "[8] Salir del sistema");

            switch (op) {

                case '1':
                    input = EntradaSalida.leerPasswordNumeral("Ingrese código de acceso único");
                    if (input.equals(codigoAcceso)) {

                        user = EntradaSalida.leerString("Nombre de usuario del Camarero.");
                        if (user.equals("")) {
                            EntradaSalida.mostrarString("ERROR: usuario no valido");
                        } else {
                            passw = EntradaSalida.leerPassword("Contraseña del Camarero.");
                            if (passw.equals("")) {
                                EntradaSalida.mostrarString("ERROR: password no valida");
                            } else {
                                Persona p = s.buscarPersona(user + ":" + passw);
                                if (p != null) {
                                    EntradaSalida.mostrarString("ERROR: El usuario ya figura en el sistema");
                                } else {
                                    nom = EntradaSalida.leerString("Nombre del Camarero");
                                    cod = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el Camarero");
                                    Camarero cama = new Camarero(user, passw, cod, nom);
                                    s.getPersonas().add(cama);
                                    EntradaSalida.mostrarString("Se ha incorporado el camarero al sistema");
                                }
                            }
                        }
                    } else {
                        EntradaSalida.mostrarString("código inválido");
                    }
                    break;

                case '2':
                    input = EntradaSalida.leerPasswordNumeral("Ingrese código de acceso único");
                    if (input.equals(codigoAcceso)) {

                        user = EntradaSalida.leerString("Nombre de usuario del Cocinero.");
                        if (user.equals("")) {
                            EntradaSalida.mostrarString("ERROR: usuario no valido");
                        } else {
                            passw = EntradaSalida.leerPassword("Contraseña del Cocinero.");
                            if (passw.equals("")) {
                                EntradaSalida.mostrarString("ERROR: password no valida");
                            } else {
                                Persona p = s.buscarPersona(user + ":" + passw);
                                if (p != null) {
                                    EntradaSalida.mostrarString("ERROR: El usuario ya figura en el sistema");
                                } else {
                                    nom = EntradaSalida.leerString("Nombre del Cocinero");
                                    cod = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el Cocinero");
                                    Cocinero coci = new Cocinero(user, passw, cod, nom);
                                    s.getPersonas().add(coci);
                                    EntradaSalida.mostrarString("Se ha incorporado el cocinero al sistema");
                                }
                            }
                        }
                    } else {
                        EntradaSalida.mostrarString("código inválido");
                    }
                    break;

                case '3':
                    input = EntradaSalida.leerPasswordNumeral("Ingrese código de acceso único");
                    if (input.equals(codigoAcceso)) {
                        String descripcion = EntradaSalida.leerString("ingrese descripción de la bebida");
                        String precio = EntradaSalida.leerString("ingrese precio de la bebida");
                        Bebida b = new Bebida(descripcion, precio);
                        s.getCarta().agregar(b);
                    } else {
                        EntradaSalida.mostrarString("código inválido");
                    }
                    break;

                case '4':
                    int opc;
                    do {
                        opc = EntradaSalida.leerInt(
                                "MENÚ PREPARACIONES\n\n"
                                + "[0] Salir\n"
                                + s.getCarta().MostrarListadoPreparaciones());
                    } while (opc < 0 || opc > (s.getCarta().obtenerCantidadPreparaciones()));
                    if (opc != 0) {
                        String precio = EntradaSalida.leerString("Plato elegido: " + s.getCarta().getPreparacion(opc - 1).getDescripcion() + "\n"
                                + "Precio actual: " + s.getCarta().getPreparacion(opc - 1).getPrecio() + "\n"
                                + "\t Por favor ingrese nuevo precio");
                        s.getCarta().getPreparacion(opc - 1).setPrecio(precio);
                    }

                    break;

                case '5':
                    s.getCarta().mostrar();
                    break;

                case '6':
                    EntradaSalida.mostrarString("Resumen de jornada del día de hoy");

                    EntradaSalida.mostrarString("Recaudación total: " + s.obtenerRecaudacion());

                    EntradaSalida.mostrarString("Preparación mas pedida: " + s.obtenerPrepMasVendida());

                    EntradaSalida.mostrarString("Bebida mas pedida: " + s.obtenerBebMasVendida());

                    EntradaSalida.mostrarString("Camarero que atendió mas pedidos:" + s.obtenerCamHighScore());

                    Boolean continuar = EntradaSalida.leerBoolean("desea reestablecer los números del día a 0 ?");
                    if (continuar == true) {
                        s.resetdia();
                    }
                    break;

                case '7':
                    seguir = true;
                    break;

                case '8':
                    seguir = false;
                    break;

                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';
            }

            if (op >= '1' && op <= '2') {
                try {
                    s.serializar("Bodegon.dat");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } while (op != '7' && op != '8');
        //}
        return seguir;
    }

}
