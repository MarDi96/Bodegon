package bodegon;

import java.io.Serializable;

public abstract class Persona implements Serializable {

    private final String usuario;
    private String password;
    private String nombre;
    private String codigoAcceso;

    public Persona(String usuario, String password, String codigoAcceso, String nombre) {
        this.usuario = usuario;
        this.password = password;
        this.codigoAcceso = codigoAcceso;
        this.nombre = nombre;
    }

    public String getCodigoAcceso() {
        return codigoAcceso;
    }

    public void setCodigoAcceso(String codigoAcceso) {
        this.codigoAcceso = codigoAcceso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public abstract boolean proceder(Sistema s);

}
