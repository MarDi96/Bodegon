/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

/**
 *
 * @author Skynet
 */
public class Bebida {

    private String descripcion;
    private String precio;
    private int vecesPedida;

    public Bebida(String descripcion, String precio) {
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getVecesPedida() {
        return vecesPedida;
    }

    public void setVecesPedida(int vecesPedida) {
        this.vecesPedida = vecesPedida;
    }

}
