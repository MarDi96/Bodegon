/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

/**
 *
 * @author Skynet
 */
public class Comanda {

    private int numeroMesa;
    private Bebida b;
    private Preparacion p;
    private String camaACargo;

    public Comanda(int numeroMesa) {  //seguramente tenga que implementar algo parecido a computacion, donde tenga un array de comandas, sino no se como tenerlas todas juntas
        this.numeroMesa = numeroMesa;
    }

    public int getNumeroMesa() {
        return numeroMesa;
    }

    public void setNumeroMesa(int numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public Bebida getB() {
        return b;
    }

    public void setB(Bebida b) {
        this.b = b;
    }

    public Preparacion getP() {
        return p;
    }

    public void setP(Preparacion p) {
        this.p = p;
    }

    public String getCamaACargo() {
        return camaACargo;
    }

    public void setCamaACargo(String camaACargo) {
        this.camaACargo = camaACargo;
    }

    public double getPrecioComanda() {
        double precio = 0;

        if (b != null) {
            precio += Double.parseDouble(b.getPrecio());
        }
        if (p != null) {
            precio += Double.parseDouble(p.getPrecio());
        }
        return precio;
    }

}
