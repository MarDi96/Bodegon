package bodegon;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Control c = new Control();
        try {
            c.ejecutar();
        } catch (NullPointerException e) {
            EntradaSalida.mostrarString(e.getMessage());
        }
        /*String archivoDatos = "Bodegon.txt";
        Sistema s = new Sistema();
        Control c = new Control(s);

        try {
            s = s.deSerializar(archivoDatos);
        } catch (IOException | ClassNotFoundException ex) {
            c.inicializacion();
        } finally {
            c.arrancar();
        }

        try {
            s.serializar(archivoDatos);
        } catch (IOException ex) {
            EntradaSalida.mostrarString(ex.getMessage());
        }*/
    }

}
