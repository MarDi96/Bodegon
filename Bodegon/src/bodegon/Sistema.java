package bodegon;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Sistema implements Serializable {

    private List<Persona> personas;
    private List<Comanda> comandas;
    private Menu carta;
    private int highScorePedidos;
    private String nomHighScore;

    public Sistema() {
        personas = new ArrayList<>();
        carta = new Menu();
        comandas = new ArrayList<>();
        highScorePedidos = 0;
        nomHighScore = "ninguno";
    }

    public Sistema deSerializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(archivo);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        o.close();
        f.close();
        return s;
    }

    public void serializar(String a) throws IOException {
        FileOutputStream f = new FileOutputStream(a);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
        f.close();
    }

    public List<Comanda> getComandas() {
        return comandas;
    }

    public Menu getCarta() {
        return carta;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public Persona buscarPersona(String datos) {
        int i = 0;
        boolean encontrado = false;
        Persona p = null;

        System.out.println(personas.size());

        while (i < personas.size() && !encontrado) {
            p = personas.get(i);
            if (datos.equals(p.getUsuario() + ":" + p.getPassword())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return p;
        }
    }

    public double obtenerRecaudacion() {
        double total = 0;
        for (int i = 0; i < comandas.size(); i++) {
            total += comandas.get(i).getPrecioComanda();
        }

        return total;
    }

    public String obtenerPrepMasVendida() {
        int max = 0;
        String prep = "ninguna";
        for (int i = 0; i < carta.getPreparaciones().size(); i++) {

            if (max < carta.getPreparacion(i).getVecesPedida()) {
                max = carta.getPreparacion(i).getVecesPedida();
                prep = carta.getPreparacion(i).getDescripcion();
            }
        }
        return prep;
    }

    public String obtenerBebMasVendida() {
        int max = 0;
        String beb = "ninguna";
        for (int i = 0; i < carta.getPreparaciones().size(); i++) {

            if (max < carta.getBebida(i).getVecesPedida()) {
                max = carta.getBebida(i).getVecesPedida();
                beb = carta.getBebida(i).getDescripcion();
            }
        }
        return beb;
    }

    public void actualizarCamHighScore(int pedidos, String nom) {
        if (pedidos > highScorePedidos) {
            highScorePedidos = pedidos;
            nomHighScore = nom;
        }
    }

    public String obtenerCamHighScore() {

        return nomHighScore;
    }

    public void resetdia() { //esta funcion actuaría como un cierre del día, haciendo que los valores de cosas como
        //numero de bebidas pedidas o la cantidad de comandas despachadas sea cero.
        int i;
        highScorePedidos = 0;
        nomHighScore = "";
        for (i = 0; i < carta.getPreparaciones().size(); i++) {
            carta.getPreparacion(i).setVecesPedida(0);
        }

        for (i = 0; i < carta.getBebidas().size(); i++) {
            carta.getBebida(i).setVecesPedida(0);
        }

        comandas.clear();

        EntradaSalida.mostrarString("Valores del día reestablecidos.");
    }


    /*public void arrancar() {
        boolean corriendo = true;

        while (corriendo) {
            Persona u = null;

            String usuario = EntradaSalida.leerString("Ingrese su usuario");
            String password = EntradaSalida.leerPassword("Ingrese password");

            for (Persona p : personas) {
                if (p.getUsuario().equals(usuario) && p.getPassword().equals(password)) {
                    u = p;
                }
            }

            if (u == null) {
                EntradaSalida.mostrarString("Usuario/contraseña inexistente");
            } else {
                corriendo = u.proceder();
            }
        }
        EntradaSalida.mostrarString("Hasta mañana");
    }*/

 /*public void inicializacion() {
        String usuario, password, nombre, codigoacceso;

        EntradaSalida.mostrarString(
                "PRIMER ARRANQUE\n\n"
                + "A continuación genere los usuarios para el sistema."
        );

        usuario = EntradaSalida.leerString("Nombre de usuario del Administrador.");
        password = EntradaSalida.leerPassword("Contraseña del Administrador.");
        nombre = EntradaSalida.leerString("Nombre del Administrador");
        codigoacceso = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el administrador");
        Administrador admin = new Administrador(usuario, password, nombre, codigoacceso);
        //admin.setSb(sb);
        //admin.setSp(sp);

        usuario = EntradaSalida.leerString("Nombre de usuario del Encargado.");
        password = EntradaSalida.leerPassword("Contraseña del Encargado.");
        Cocinero coci = new Cocinero(usuario, password, nombre, codigoacceso);
        //e.setSp(sp);

        usuario = EntradaSalida.leerString("Nombre de usuario del Vendedor.");
        password = EntradaSalida.leerPassword("Contraseña del Vendedor.");
        Camarero cama = new Camarero(usuario, password, nombre, codigoacceso);
        //v.setSb(sb);

        personas.add(admin);
        personas.add(coci);
        personas.add(cama);

        EntradaSalida.mostrarString(
                "USUARIOS CARGADOS\n\n"
                + "Ya puede ingresar al sistema."
        );
    }*/
}
