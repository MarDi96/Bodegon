package bodegon;

import java.io.IOException;

public class Control {

    Sistema s;

    public void arrancar() {

        boolean corriendo = true;

        while (corriendo) {
            Persona u = null;

            String usuario = EntradaSalida.leerString("Ingrese su usuario");
            String password = EntradaSalida.leerPassword("Ingrese password");

            u = s.buscarPersona(usuario + ":" + password);

            if (u == null) {
                EntradaSalida.mostrarString("ERROR: La combinacion usuario/password ingresada no es valida.");
            } else {
                corriendo = u.proceder(s);
            }
            try {
                s.serializar("Bodegon.dat");
            } catch (IOException ex) {
                EntradaSalida.mostrarString(ex.getMessage());
            }          
        }
        EntradaSalida.mostrarString("Hasta mañana");
    }

    public void inicializacion() {
        String usuario, password, nombre, codigoacceso;

        EntradaSalida.mostrarString(
                "PRIMER ARRANQUE\n\n"
                + "A continuación genere los usuarios para el sistema."
        );

        usuario = EntradaSalida.leerString("Nombre de usuario del Administrador.");
        password = EntradaSalida.leerPassword("Contraseña del Administrador.");
        nombre = EntradaSalida.leerString("Nombre del Administrador");
        codigoacceso = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el administrador");
        Administrador admin = new Administrador(usuario, password, codigoacceso, nombre);
        //admin.setSb(sb);
        //admin.setSp(sp);

        usuario = EntradaSalida.leerString("Nombre de usuario del Cocinero.");
        password = EntradaSalida.leerPassword("Contraseña del Cocinero.");
        nombre = EntradaSalida.leerString("Nombre del Cocinero");
        codigoacceso = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el Cocinero");
        Cocinero coci = new Cocinero(usuario, password, codigoacceso, nombre);
        //e.setSp(sp);

        usuario = EntradaSalida.leerString("Nombre de usuario del camarero.");
        password = EntradaSalida.leerPassword("Contraseña del camarero.");
        nombre = EntradaSalida.leerString("Nombre del camarero");
        codigoacceso = EntradaSalida.leerPasswordNumeral("Codigo de acceso único para el camarero");
        Camarero cama = new Camarero(usuario, password, codigoacceso, nombre);
        //v.setSb(sb);

        s.getPersonas().add(admin);
        s.getPersonas().add(coci);
        s.getPersonas().add(cama);

        EntradaSalida.mostrarString(
                "USUARIOS CARGADOS\n\n"
                + "Ya puede ingresar al sistema."
        );
    }

    void ejecutar() {
        String archivoDatos = "Bodegon.dat";
        s = new Sistema();
        try {
            s = s.deSerializar(archivoDatos);
        } catch (IOException | ClassNotFoundException ex) {
            inicializacion(); //parece medio C hacer esto, pero es preferible antes que copypastear el código adentro de esta función
        } finally {
            arrancar();
        }
        try {
            s.serializar(archivoDatos);
        } catch (IOException ex) {
            EntradaSalida.mostrarString(ex.getMessage());
        }
    }
}
