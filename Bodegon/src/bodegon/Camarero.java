/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

/**
 *
 * @author Skynet
 */
public class Camarero extends Persona {

    private String nombre;
    private String codigoAcceso;
    private String password;
    private String usuario;
    private int pedidosTomados;

    public Camarero(String usuario, String password, String codigoAcceso, String nombre) {
        super(usuario, password, codigoAcceso, nombre);
        this.nombre = nombre;
        this.codigoAcceso = codigoAcceso;
        this.usuario = usuario;
        this.password = password;
        pedidosTomados = 0;
    }

    @Override
    public boolean proceder(Sistema s) {
        int numesa, opc, aux;
        boolean seguir = true;
        char op;
        do {
            op = EntradaSalida.leerChar(
                    "Hola, " + nombre + ".\n"
                    + "MENÚ CAMAREROS\n\n"
                    + "[1] Cargar comanda\n"
                    + "[2] Cerrar sesión\n"
                    + "[3] Salir del sistema");

            switch (op) {

                case '1':
                    String input;
                    input = EntradaSalida.leerPasswordNumeral("Ingrese código de acceso único");
                    if (input.equals(codigoAcceso)) {

                        numesa = EntradaSalida.leerInt("Ingrese el número de mesa");
                        Comanda com = new Comanda(numesa);
                        com.setCamaACargo(nombre);

                        boolean continuar = true;
                        while (continuar) {
                            opc = EntradaSalida.leerInt(
                                    "CARTA DE PLATOS DE HOY\n\n"
                                    + "[0] Ninguno\n"
                                    + s.getCarta().MostrarListadoPreparacionesVendibles()); //seteo de plato, 0 para nada

                            if (opc != 0) {
                                aux = s.getCarta().getPreparacion(opc - 1).getVecesPedida();
                                aux++;
                                s.getCarta().getPreparacion(opc - 1).setVecesPedida(aux); //esto es horrible, pero no se me ocurre otra cosa por el momento
                                com.setP(s.getCarta().getPreparacion(opc - 1));
                            }

                            opc = EntradaSalida.leerInt(
                                    "CARTA DE BEBIDAS DE HOY \n\n"
                                    + "[0]Ninguno\n"
                                    + s.getCarta().MostrarListadoBebidas()); //seteo de bebida, 0 para nada

                            if (opc != 0) {
                                aux = s.getCarta().getBebida(opc - 1).getVecesPedida();
                                aux++;
                                s.getCarta().getBebida(opc - 1).setVecesPedida(aux);
                                com.setB(s.getCarta().getBebida(opc - 1));
                            }

                            s.getComandas().add(com); //comanda creada

                            EntradaSalida.mostrarString("comanda " + com + " creada");

                            continuar = EntradaSalida.leerBoolean("desea continuar agregando pedidos de la mesa " + numesa + "?");
                        }
                        pedidosTomados++;
                    } else {
                        EntradaSalida.mostrarString("código inválido");
                    }

                    break;

                case '2':
                    seguir = true;
                    break;
                case '3':
                    seguir = false;
                    break;
                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';
            }

        } while (op != '2' && op != '3');
        s.actualizarCamHighScore(pedidosTomados, nombre);
        return seguir;
    }

    public int getPedidosTomados() {
        return pedidosTomados;
    }

    public void setPedidosTomados(int pedidosTomados) {
        this.pedidosTomados = pedidosTomados;
    }

}
