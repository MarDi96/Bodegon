/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodegon;

/**
 *
 * @author Skynet
 */
public class Cocinero extends Persona {

    private String nombre;
    private String codigoAcceso;
    private String password;
    private String usuario;

    public Cocinero(String usuario, String password, String codigoAcceso, String nombre) {
        super(usuario, password, codigoAcceso, nombre);
        this.nombre = nombre;
        this.codigoAcceso = codigoAcceso;
        this.usuario = usuario;
        this.password = password;
    }

    @Override
    public boolean proceder(Sistema s) {
        boolean seguir = true;
        char op;
        do {
            op = EntradaSalida.leerChar(
                    "Hola, " + nombre + ".\n"
                    + "MENÚ COCINEROS\n\n"
                    + "[1] Cargar preparacion\n"
                    + "[2] Cerrar sesión\n"
                    + "[3] Salir del sistema");

            switch (op) {

                case '1':
                    String input;
                    input = EntradaSalida.leerPasswordNumeral("Ingrese código de acceso único");
                    if (input.equals(codigoAcceso)) {
                        boolean continuar = true;
                        while (continuar) {
                            String descripcion = EntradaSalida.leerString("Ingrese descripción de la preparación");
                            //String precio = EntradaSalida.leerString("Ingrese precio de la preparación"); no se le puede poner precio desde acá.
                            Preparacion p = new Preparacion(descripcion, "0");
                            s.getCarta().agregar(p);

                            continuar = EntradaSalida.leerBoolean("desea continuar?");
                        }
                    } else {
                        EntradaSalida.mostrarString("código inválido");
                    }

                    break;
                case '2':
                    seguir = true;
                    break;
                case '3':
                    seguir = false;
                    break;

                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';
                    break;
            }

        } while (op != '2' && op != '3');
        return seguir;
    }

}
